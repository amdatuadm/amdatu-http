/*
 * Copyright (c) OSGi Alliance (2000, 2014). All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osgi.service.http;

/**
 * Defines standard names for Http Service constants.
 * 
 * @since 1.3
 * @author $Id$
 */
public final class HttpConstants {

    public static final String HTTP_WHITEBOARD_CONTEXT_NAME = "osgi.http.whiteboard.context.name";

    public static final String HTTP_WHITEBOARD_CONTEXT_SELECT = "osgi.http.whiteboard.context.select";

    public static final String HTTP_WHITEBOARD_CONTEXT_SHARED = "osgi.http.whiteboard.context.shared";

    public static final String HTTP_WHITEBOARD_SERVLET_NAME = "osgi.http.whiteboard.servlet.name";

    public static final String HTTP_WHITEBOARD_SERVLET_PATTERN = "osgi.http.whiteboard.servlet.pattern";

    public static final String HTTP_WHITEBOARD_SERVLET_ERROR_PAGE = "osgi.http.whiteboard.servlet.errorPage";

    public static final String HTTP_WHITEBOARD_SERVLET_ASYNC_SUPPORTED = "osgi.http.whiteboard.servlet.asyncSupported";

    public static final String HTTP_WHITEBOARD_FILTER_NAME = "osgi.http.whiteboard.filter.name";

    public static final String HTTP_WHITEBOARD_FILTER_PATTERN = "osgi.http.whiteboard.filter.pattern";

    public static final String HTTP_WHITEBOARD_FILTER_SERVLET = "osgi.http.whiteboard.filter.servlet";

    public static final String HTTP_WHITEBOARD_FILTER_ASYNC_SUPPORTED = "osgi.http.whiteboard.filter.asyncSupported";

    public static final String HTTP_WHITEBOARD_FILTER_DISPATCHER = "osgi.http.whiteboard.filter.dispatcher";

    public static final String DISPATCHER_REQUEST = "REQUEST";

    public static final String DISPATCHER_INCLUDE = "INCLUDE";

    public static final String DISPATCHER_FORWARD = "FORWARD";

    public static final String DISPATCHER_ASYNC = "ASYNC";

    public static final String DISPATCHER_ERROR = "ERROR";

    public static final String HTTP_WHITEBOARD_RESOURCE_PREFIX = "osgi.http.whiteboard.resource.prefix";

    public static final String HTTP_WHITEBOARD_TARGET = "osgi.http.whiteboard.target";

    public static final String HTTP_SERVICE_ENDPOINT_ATTRIBUTE = "osgi.http.endpoint";

    private HttpConstants() {

    }
}
