/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.spdy;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.http.HttpConnectorProvider;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Provides the OSGi bundle activator.
 */
public class Activator implements BundleActivator {
    /**
     * Qualified class name of the NPN SPI that should be available through the JVM bootclasspath.
     */
    private static final String NPN_NEXT_PROTO_NEGO = "org.eclipse.jetty.npn.NextProtoNego";

    @Override
    public void start(BundleContext context) throws Exception {
        // Will throw an exception (an thus not register our service)...
        try {
            Class<?> npn = ClassLoader.getSystemClassLoader().loadClass(NPN_NEXT_PROTO_NEGO);
            if (npn.getClassLoader() != null) {
                throw new IllegalStateException("NextProtoNego must be on JVM boot path");
            }
        }
        catch (ClassNotFoundException e) {
            throw new IllegalStateException("No NextProtoNego on boot path", e);
        }

        Dictionary<String, Object> props = new Hashtable<>();
        props.put("types", new String[] { "spdy", "spdy2", "spdy3" });
        context.registerService(HttpConnectorProvider.class, new SpdyHttpConnectorProvider(), props);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        // Nop
    }
}
