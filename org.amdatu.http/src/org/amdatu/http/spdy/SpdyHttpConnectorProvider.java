/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.spdy;

import static org.amdatu.http.AmdatuHttpConstants.CONFIG_KEYSTORE_INCLUDE_PROTOCOLS;
import static org.amdatu.http.AmdatuHttpConstants.CONFIG_KEYSTORE_KEY_PWD;
import static org.amdatu.http.AmdatuHttpConstants.CONFIG_KEYSTORE_PATH;
import static org.amdatu.http.AmdatuHttpConstants.CONFIG_KEYSTORE_PROTOCOL;
import static org.amdatu.http.AmdatuHttpConstants.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Dictionary;
import java.util.List;
import java.util.Set;

import org.amdatu.http.HttpConnectorProvider;
import org.eclipse.jetty.server.ConnectionFactory;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.spdy.server.NPNServerConnectionFactory;
import org.eclipse.jetty.spdy.server.http.HTTPSPDYServerConnectionFactory;
import org.eclipse.jetty.spdy.server.http.ReferrerPushStrategy;
import org.eclipse.jetty.util.ssl.SslContextFactory;

/**
 * Connector provider for SPDY-support.
 */
public class SpdyHttpConnectorProvider implements HttpConnectorProvider {

    @Override
    public void handle(Set<String> types, Dictionary<String, ?> config, List<ConnectionFactory> factories) {
        if (!types.contains(TYPE_SPDY) && !types.contains(TYPE_SPDYv2) && !types.contains(TYPE_SPDYv3)) {
            // We're not used...
            return;
        }

        HttpConfiguration secureConfig = new HttpConfiguration();
        secureConfig.addCustomizer(new SecureRequestCustomizer());

        HttpConnectionFactory http = findConnectionFactory(factories, HttpConnectionFactory.class);
        SslConnectionFactory https = findConnectionFactory(factories, SslConnectionFactory.class);
        HTTPSPDYServerConnectionFactory spdyV2 = null;
        HTTPSPDYServerConnectionFactory spdyV3 = null;

        if (http == null) {
            // To fall back to HTTPS for browsers that do not support SPDY we
            // always add the default HTTP connector...
            http = new HttpConnectionFactory(secureConfig);
            factories.add(0, http);
        }

        if (types.contains(TYPE_SPDY) || types.contains(TYPE_SPDYv2)) {
            spdyV2 = new HTTPSPDYServerConnectionFactory(2, secureConfig);
            factories.add(0, spdyV2);
        }
        if (types.contains(TYPE_SPDY) || types.contains(TYPE_SPDYv3)) {
            spdyV3 = new HTTPSPDYServerConnectionFactory(3, secureConfig, new ReferrerPushStrategy());
            factories.add(0, spdyV3);
        }

        if (spdyV2 != null || spdyV3 != null) {
            List<String> protocols = new ArrayList<String>();
            // Protocols appear to be used in order defined...
            if (spdyV3 != null) {
                protocols.add(spdyV3.getProtocol());
            }
            if (spdyV2 != null) {
                protocols.add(spdyV2.getProtocol());
            }
            protocols.add(http.getProtocol());

            NPNServerConnectionFactory npn =
                new NPNServerConnectionFactory(protocols.toArray(new String[protocols.size()]));
            // In case SPDY is *not* supported, we fall back on plain HTTP...
            npn.setDefaultProtocol(http.getProtocol());

            factories.add(0, npn);

            if (https == null) {
                SslContextFactory factory = new SslContextFactory();
                factory.setKeyStorePath((String) config.get(CONFIG_KEYSTORE_PATH));
                factory.setKeyStorePassword((String) config.get(CONFIG_KEYSTORE_STORE_PWD));
                factory.setKeyManagerPassword((String) config.get(CONFIG_KEYSTORE_KEY_PWD));
                Object value = config.get(CONFIG_KEYSTORE_INCLUDE_PROTOCOLS);
                if (value != null) {
                    factory.setIncludeProtocols(parseStringCollection(value));
                }
                value = config.get(CONFIG_KEYSTORE_PROTOCOL);
                if (value != null) {
                    factory.setProtocol((String) value);
                }

                https = new SslConnectionFactory(factory, npn.getProtocol());
                factories.add(0, https);
            }
        }
    }

    private <T extends ConnectionFactory> T findConnectionFactory(List<ConnectionFactory> factories, Class<T> type) {
        for (ConnectionFactory factory : factories) {
            if (type.isInstance(factory)) {
                return (T) factory;
            }
        }
        return null;
    }

    private String[] parseStringCollection(Object value) {
        if (value instanceof String) {
            return new String[] { (String) value };
        }
        else if (value instanceof String[]) {
            return (String[]) value;
        }
        else if (value instanceof Collection) {
            Collection coll = (Collection) value;
            return (String[]) coll.toArray(new String[coll.size()]);
        }
        return null;
    }
}
