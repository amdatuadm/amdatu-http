/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http;

/**
 * Constants for the Amdatu HTTP service implementation.
 */
public interface AmdatuHttpConstants {
    /**
     * Factory PID to configure new connectors in Amdatu-HTTP.
     */
    String FACTORY_PID = "org.amdatu.http.factory";

    /**
     * Configuration property to denote the listening port. Use <tt>0</tt> to automatically assign a port.
     */
    String CONFIG_PORT = "port";
    /**
     * Configuration property to denote the host the server should listen on. Use <tt>0.0.0.0</tt> to bind to all interfaces.
     */
    String CONFIG_HOST = "host";
    /**
     * Configuration property to denote what type of connections are to be supported, for example "http", "https" or "spdy". See <tt>TYPE_*</tt> constants.
     */
    String CONFIG_TYPES = "types";
    /**
     * Configuration property to denote the path to the keystore used for HTTPS/SPDY.
     */
    String CONFIG_KEYSTORE_PATH = "keystore.path";
    /**
     * Configuration property to denote the password used to access the keystore.
     */
    String CONFIG_KEYSTORE_STORE_PWD = "keystore.store.password";
    /**
     * Configuration property to denote the password used to access the key in the keystore.
     */
    String CONFIG_KEYSTORE_KEY_PWD = "keystore.key.password";
    /**
     * Configuration property to denote what protocols should be supported over a SSL-connection.
     */
    String CONFIG_KEYSTORE_INCLUDE_PROTOCOLS = "keystore.include.protocols";
    /**
     * Configuration property to denote what protocol should be used over a SSL-connection.
     */
    String CONFIG_KEYSTORE_PROTOCOL = "keystore.protocol";

    /**
     * To define a plain HTTP connector.
     */
    String TYPE_HTTP = "http";
    /**
     * To define a HTTPS connector.
     */
    String TYPE_HTTPS = "https";
    /**
     * To define a SPDY connector (supporting both v2 and v3).
     */
    String TYPE_SPDY = "spdy";
    /**
     * To define a SPDY v2 connector.
     */
    String TYPE_SPDYv2 = "spdy2";
    /**
     * To define a SPDY v3 connector.
     */
    String TYPE_SPDYv3 = "spdy3";

    /**
     * To support Servlets registered with the Felix HTTP-service properties.
     */
    String FELIX_SERVLET_ALIAS = "alias";
    /**
     * To support Filters registered with the Felix HTTP-service properties.
     */
    String FELIX_FILTER_PATTERN = "pattern";
}
