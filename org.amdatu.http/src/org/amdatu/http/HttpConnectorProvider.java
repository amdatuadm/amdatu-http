/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http;

import java.util.Dictionary;
import java.util.List;
import java.util.Set;

import org.eclipse.jetty.server.ConnectionFactory;

/**
 * TODO sensible name?!
 */
public interface HttpConnectorProvider {

    void handle(Set<String> providerTypes, Dictionary<String, ?> config, List<ConnectionFactory> factories);
}
