/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.jetty;

import java.io.IOException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.http.HttpContext;
import org.osgi.service.http.ServletContextHelper;

/**
 * Provides an adapter for {@link HttpContext}.
 */
public class HttpContextAdapter extends ServletContextHelper implements HttpContext {
    private final HttpContext m_delegate;

    public HttpContextAdapter(HttpContext delegate) {
        m_delegate = delegate;
    }

    @Override
    public String getMimeType(String name) {
        return m_delegate.getMimeType(name);
    }

    @Override
    public URL getResource(String name) {
        return m_delegate.getResource(name);
    }

    @Override
    public boolean handleSecurity(HttpServletRequest request, HttpServletResponse response) throws IOException {
        return m_delegate.handleSecurity(request, response);
    }
}
