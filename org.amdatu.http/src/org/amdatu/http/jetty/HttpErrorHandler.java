/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.jetty;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpMethod;
import org.eclipse.jetty.http.MimeTypes;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Response;
import org.eclipse.jetty.server.handler.ErrorHandler;
import org.eclipse.jetty.util.ByteArrayISO8859Writer;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;
import org.osgi.framework.ServiceReference;

/**
 * Custom error handling by allowing specific {@link Servlet}s to be called in case of errors.
 */
public class HttpErrorHandler extends ErrorHandler {
    private static final Logger LOG = Log.getLogger(HttpErrorHandler.class);

    private final ConcurrentMap<String, List<Servlet>> m_mappings;

    /**
     * Creates a new {@link HttpErrorHandler} instance.
     */
    public HttpErrorHandler() {
        m_mappings = new ConcurrentHashMap<>();
    }

    public void addErrorServlet(ServiceReference<?> reference, Servlet servlet, String... errorPages) {
        for (String errorPage : errorPages) {
            List<Servlet> servlets = m_mappings.get(errorPage);
            if (servlets == null) {
                servlets = new CopyOnWriteArrayList<>();

                List<Servlet> putResult = m_mappings.putIfAbsent(errorPage, servlets);
                if (putResult != null) {
                    // Lost the put...
                    servlets = putResult;
                }
            }
            servlets.add(servlet);
        }
    }

    public void removeErrorServlet(ServiceReference<?> reference, Servlet servlet, String... errorPages) {
        for (String errorPage : errorPages) {
            List<Servlet> servlets = m_mappings.get(errorPage);
            if (servlets != null) {
                servlets.remove(servlet);
                if (servlets.isEmpty()) {
                    m_mappings.remove(errorPage, servlets);
                }
            }
        }
    }

    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
        throws IOException {
        String method = request.getMethod();
        if (!HttpMethod.GET.is(method) && !HttpMethod.POST.is(method) && !HttpMethod.HEAD.is(method)) {
            baseRequest.setHandled(true);
        }

        if (baseRequest.isHandled()) {
            // Avoid infinite loops...
            return;
        }

        baseRequest.setHandled(true);

        String key;

        Class<?> exceptionType = (Class<?>) request.getAttribute(RequestDispatcher.ERROR_EXCEPTION_TYPE);
        if (exceptionType != null) {
            // TODO RFC-189: exact types only, or are subtypes also to be handled?
            key = exceptionType.getName();
        }
        else {
            key = Integer.toString(response.getStatus());
        }

        List<Servlet> servlets = m_mappings.get(key);
        if (servlets != null && !servlets.isEmpty()) {
            try {
                // Start over...
                response.reset();

                // TODO RFC-189 is not conclusive what to do with multiple mappings...
                servlets.get(0).service(request, response);
                return;
            }
            catch (ServletException e) {
                // Fall back to default behaviour...
                LOG.warn("Custom error handler failed!", e);
            }
        }

        response.setContentType(MimeTypes.Type.TEXT_HTML_8859_1.asString());
        ByteArrayISO8859Writer writer = new ByteArrayISO8859Writer(4096);
        String reason = (response instanceof Response) ? ((Response) response).getReason() : null;
        handleErrorPage(request, writer, response.getStatus(), reason);
        writer.flush();
        response.setContentLength(writer.size());
        writer.writeTo(response.getOutputStream());
        writer.destroy();
    }
}
