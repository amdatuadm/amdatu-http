/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.jetty;

import java.io.PrintStream;
import java.util.Arrays;

import org.eclipse.jetty.util.log.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;

public class HttpLogger extends ServiceTracker<LogService, LogService> implements Logger {
    private final PrintStream m_out;
    private volatile boolean m_debugEnabled;

    public HttpLogger(BundleContext context) {
        super(context, LogService.class, null);

        m_out = System.out;
    }

    @Override
    public void debug(String msg, long value) {
        if (m_debugEnabled) {
            LogService log = getService();
            if (log != null) {
                log.log(LogService.LOG_DEBUG, msg + " " + value);
            }
            else {
                m_out.printf("[DEBUG] %s %d%n", msg, value);
            }
        }
    }

    public void debug(String msg, Object... args) {
        if (m_debugEnabled) {
            LogService log = getService();
            if (log != null) {
                log.log(LogService.LOG_DEBUG, format(msg, args));
            }
            else {
                m_out.printf("[DEBUG] %s%n", format(msg, args));
            }
        }
    }

    public void debug(String msg, Throwable throwable) {
        if (m_debugEnabled) {
            LogService log = getService();
            if (log != null) {
                log.log(LogService.LOG_DEBUG, msg, throwable);
            }
            else {
                m_out.printf("[DEBUG] %s: %s%n", msg, throwable.getMessage());
                throwable.printStackTrace(m_out);
            }
        }
    }

    public void debug(Throwable throwable) {
        if (m_debugEnabled) {
            LogService log = getService();
            if (log != null) {
                log.log(LogService.LOG_DEBUG, "", throwable);
            }
            else {
                m_out.printf("[DEBUG] Exception caught: %s%n", throwable.getMessage());
                throwable.printStackTrace(m_out);
            }
        }
    }

    public Logger getLogger(String name) {
        return this;
    }

    public String getName() {
        return "HttpLogger";
    }

    public void ignore(Throwable throwable) {
        if (m_debugEnabled) {
            m_out.printf("Ignoring: %s.%n", throwable.getMessage());
        }
    }

    public void info(String msg, Object... args) {
        LogService log = getService();
        if (log != null) {
            log.log(LogService.LOG_INFO, format(msg, args));
        }
        else if (m_debugEnabled) {
            m_out.printf("[INFO ] %s%n", format(msg, args));
        }
    }

    public void info(String msg, Throwable throwable) {
        LogService log = getService();
        if (log != null) {
            log.log(LogService.LOG_INFO, msg, throwable);
        }
        else if (m_debugEnabled) {
            m_out.printf("[INFO ] %s: %s%n", msg, throwable.getMessage());
            throwable.printStackTrace(m_out);
        }
    }

    public void info(Throwable throwable) {
        LogService log = getService();
        if (log != null) {
            log.log(LogService.LOG_INFO, "", throwable);
        }
        else if (m_debugEnabled) {
            m_out.printf("[INFO ] Exception caught: %s%n", throwable.getMessage());
            throwable.printStackTrace(m_out);
        }
    }

    public boolean isDebugEnabled() {
        return m_debugEnabled;
    }

    public void setDebugEnabled(boolean enabled) {
        m_debugEnabled = enabled;
    }

    public void warn(String msg, Object... args) {
        LogService log = getService();
        if (log != null) {
            log.log(LogService.LOG_WARNING, format(msg, args));
        }
        else if (m_debugEnabled) {
            m_out.printf("[WARN ] %s%n", format(msg, args));
        }
    }

    public void warn(String msg, Throwable throwable) {
        LogService log = getService();
        if (log != null) {
            log.log(LogService.LOG_WARNING, msg, throwable);
        }
        else if (m_debugEnabled) {
            m_out.printf("[WARN ] %s: %s%n", msg, throwable.getMessage());
            throwable.printStackTrace(m_out);
        }
    }

    public void warn(Throwable throwable) {
        LogService log = getService();
        if (log != null) {
            log.log(LogService.LOG_WARNING, "", throwable);
        }
        else if (m_debugEnabled) {
            m_out.printf("[WARN ] Exception caught: %s%n", throwable.getMessage());
            throwable.printStackTrace(m_out);
        }
    }

    private String format(String msg, Object... args) {
        StringBuilder sb = new StringBuilder();
        if (msg == null) {
            for (int i = 0; i < args.length; i++) {
                if (i > 0) {
                    sb.append(" ");
                }
                sb.append(args[i]);
            }
        }
        else {
            try {
                int idx, prevIdx = 0, argIdx = 0;
                while ((idx = msg.indexOf("{}", prevIdx)) >= 0) {
                    sb.append(msg.substring(prevIdx, idx));
                    if (argIdx < args.length) {
                        sb.append(args[argIdx++]);
                    }
                    prevIdx = idx + 2;
                }
                if (prevIdx >= 0 && prevIdx < msg.length()) {
                    sb.append(msg.substring(prevIdx));
                }
            }
            catch (Exception e) {
                System.err.println(e.getMessage() + " for '" + msg + "' & " + Arrays.toString(args));
            }
        }
        return sb.toString();
    }
}
