/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.jetty;

import static org.amdatu.http.AmdatuHttpConstants.FELIX_FILTER_PATTERN;
import static org.amdatu.http.AmdatuHttpConstants.FELIX_SERVLET_ALIAS;
import static org.amdatu.http.jetty.Util.getBooleanProperty;
import static org.amdatu.http.jetty.Util.getIntProperty;
import static org.amdatu.http.jetty.Util.getStringCollectionProperty;
import static org.amdatu.http.jetty.Util.getStringProperty;
import static org.amdatu.http.jetty.Util.removeElementAt;
import static org.osgi.service.http.HttpConstants.HTTP_WHITEBOARD_FILTER_ASYNC_SUPPORTED;
import static org.osgi.service.http.HttpConstants.HTTP_WHITEBOARD_FILTER_DISPATCHER;
import static org.osgi.service.http.HttpConstants.HTTP_WHITEBOARD_FILTER_NAME;
import static org.osgi.service.http.HttpConstants.HTTP_WHITEBOARD_FILTER_PATTERN;
import static org.osgi.service.http.HttpConstants.HTTP_WHITEBOARD_FILTER_SERVLET;
import static org.osgi.service.http.HttpConstants.HTTP_WHITEBOARD_SERVLET_ASYNC_SUPPORTED;
import static org.osgi.service.http.HttpConstants.HTTP_WHITEBOARD_SERVLET_ERROR_PAGE;
import static org.osgi.service.http.HttpConstants.HTTP_WHITEBOARD_SERVLET_NAME;
import static org.osgi.service.http.HttpConstants.HTTP_WHITEBOARD_SERVLET_PATTERN;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.Set;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterRegistration;
import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.eclipse.jetty.servlet.BaseHolder.Source;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.FilterMapping;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlet.ServletMapping;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.ServletContextHelper;

/**
 * Extension to {@link ServletContextHandler} to allow removal of {@link Servlet}s and {@link Filter}s.
 */
public class HttpServletContextHandler extends ServletContextHandler {
    /**
     * Wraps a {@link ServletContextHelper} as {@link ServletContext}.
     */
    final class ContextWrapper extends Context {
        private ServletContextHelper m_helper;

        public ContextWrapper(ServletContextHelper helper) {
            m_helper = helper;
        }

        @Override
        public FilterRegistration.Dynamic addFilter(String filterName, Class<? extends Filter> filterClass) {
            throw new UnsupportedOperationException();
        }

        @Override
        public FilterRegistration.Dynamic addFilter(String filterName, Filter filter) {
            throw new UnsupportedOperationException();
        }

        @Override
        public FilterRegistration.Dynamic addFilter(String filterName, String className) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void addListener(Class<? extends EventListener> listenerClass) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void addListener(String className) {
            throw new UnsupportedOperationException();
        }

        @Override
        public <T extends EventListener> void addListener(T t) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ServletRegistration.Dynamic addServlet(String servletName, Class<? extends Servlet> servletClass) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ServletRegistration.Dynamic addServlet(String servletName, Servlet servlet) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ServletRegistration.Dynamic addServlet(String servletName, String className) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ClassLoader getClassLoader() {
            return HttpServletContextHandler.class.getClassLoader();
        }

        @Override
        public String getInitParameter(String name) {
            return super.getInitParameter(name); // XXX see note 2
        }

        @Override
        public Enumeration<String> getInitParameterNames() {
            return super.getInitParameterNames(); // XXX see note 2
        }

        @Override
        public String getMimeType(String file) {
            return m_helper.getMimeType(file);
        }

        @Override
        public RequestDispatcher getNamedDispatcher(String name) {
            return super.getNamedDispatcher(name); // XXX see note 1
        }

        @Override
        public String getRealPath(String path) {
            return m_helper.getRealPath(path);
        }

        @Override
        public RequestDispatcher getRequestDispatcher(String name) {
            return super.getRequestDispatcher(name); // XXX see note 1
        }

        @Override
        public URL getResource(String path) throws MalformedURLException {
            return m_helper.getResource(path);
        }

        @Override
        public InputStream getResourceAsStream(String path) {
            try {
                URL url = getResource(path);
                if (url == null) {
                    return null;
                }
                return url.openStream();
            }
            catch (Exception e) {
                return null;
            }
        }

        @Override
        public Set<String> getResourcePaths(String path) {
            return m_helper.getResourcePaths(path);
        }

        @Override
        public String getServletContextName() {
            return getDisplayName();
        }
    }

    /**
     * Provides an OSGi-aware variant of {@link ServletHandler}, capable of removing servlets and filters.
     */
    static class HttpServletHandler extends ServletHandler {

        public HttpServletHandler() {
            setFilterChainsCached(false);
        }

        public synchronized void addFilter(int position, FilterHolder filter, FilterMapping filterMapping) {
            super.addFilter(filter);
            // TODO this can be made a little more smarter?
            super.setFilterMappings(super.insertFilterMapping(filterMapping, position, true /* before */));
        }

        public synchronized void addServlet(ServletHolder servlet, ServletMapping servletMapping) {
            super.addServlet(servlet);
            super.addServletMapping(servletMapping);
        }

        public synchronized void removeFilter(String filterName) {
            removeFilterHolder(filterName);
            removeFilterMapping(filterName);
        }

        public synchronized void removeServlet(String servletName) {
            removeServletHolder(servletName);
            removeServletMapping(servletName);
        }

        private void removeFilterHolder(String filterName) {
            FilterHolder[] holders = getFilters();

            int idx = -1;
            for (int i = 0; i < holders.length; i++) {
                if (filterName.equals(holders[i].getName())) {
                    idx = i;
                    break;
                }
            }
            if (idx >= 0) {
                setFilters(removeElementAt(holders, idx));

                try {
                    holders[idx].stop();
                }
                catch (Exception e) {
                    LOG.warn("Stopping filter caused an exception!", e);
                }
            }
        }

        private void removeFilterMapping(String filterName) {
            FilterMapping[] mappings = getFilterMappings();

            int idx = -1;
            for (int i = 0; i < mappings.length; i++) {
                if (filterName.equals(mappings[i].getFilterName())) {
                    idx = i;
                    break;
                }
            }
            if (idx >= 0) {
                setFilterMappings(removeElementAt(mappings, idx));
            }
        }

        private void removeServletHolder(String servletName) {
            ServletHolder[] holders = getServlets();

            int idx = -1;
            for (int i = 0; i < holders.length; i++) {
                if (servletName.equals(holders[i].getName())) {
                    idx = i;
                    break;
                }
            }
            if (idx >= 0) {
                setServlets(removeElementAt(holders, idx));

                try {
                    holders[idx].stop();
                }
                catch (Exception e) {
                    LOG.warn("Stopping servlet caused an exception!", e);
                }
            }
        }

        private void removeServletMapping(String servletName) {
            ServletMapping[] mappings = getServletMappings();

            int idx = -1;
            for (int i = 0; i < mappings.length; i++) {
                if (servletName.equals(mappings[i].getServletName())) {
                    idx = i;
                    break;
                }
            }
            if (idx >= 0) {
                setServletMappings(removeElementAt(mappings, idx));
            }
        }
    }

    private static final Logger LOG = Log.getLogger(HttpServletContextHandler.class);

    private final ServletContextHelper m_contextHelper;

    /**
     * Creates a new {@link HttpServletContextHandler} instance.
     */
    public HttpServletContextHandler(ServletContextHelper helper) {
        super(ServletContextHandler.SESSIONS);

        m_contextHelper = helper;
        _scontext = new ContextWrapper(m_contextHelper);
    }

    public boolean addFilter(ServiceReference<?> reference, Filter filter) {
        int ranking = getIntProperty(reference, Constants.SERVICE_RANKING, 0);
        boolean async = getBooleanProperty(reference, HTTP_WHITEBOARD_FILTER_ASYNC_SUPPORTED);
        String[] dispatchers = getStringCollectionProperty(reference, HTTP_WHITEBOARD_FILTER_DISPATCHER);
        String[] patterns = getStringCollectionProperty(reference, HTTP_WHITEBOARD_FILTER_PATTERN);
        if (patterns == null || patterns.length == 0) {
            // Check whether we've got a Servlet registration from the old Felix HTTP service...
            String pattern = getStringProperty(reference, FELIX_FILTER_PATTERN);
            if (pattern != null) {
                patterns = new String[] { pattern };
            }
            else {
                // No alias or pattern defined, this is needed for filters...
                return false;
            }
        }
        String[] servletNames = getStringCollectionProperty(reference, HTTP_WHITEBOARD_FILTER_SERVLET);
        String name = getStringProperty(reference, HTTP_WHITEBOARD_FILTER_NAME);
        if (name == null) {
            // See RFC-189, 5.2.5...
            name = filter.getClass().getName();
        }

        EnumSet<DispatcherType> dispatches = EnumSet.of(DispatcherType.REQUEST);
        if (dispatchers != null) {
            for (String dispatcher : dispatchers) {
                dispatches.add(DispatcherType.valueOf(dispatcher));
            }
        }

        FilterMapping mapping = new FilterMapping();
        mapping.setDispatcherTypes(dispatches);
        mapping.setServletNames(servletNames);
        mapping.setFilterName(name);
        mapping.setPathSpecs(patterns);

        FilterHolder holder = new FilterHolder(Source.EMBEDDED);
        holder.setAsyncSupported(async);
        holder.setFilter(filter);
        holder.setName(name);

        getServletHandler().addFilter(ranking, holder, mapping);

        return true;
    }

    public boolean addServlet(ServiceReference<?> reference, Servlet servlet) {
        boolean async = getBooleanProperty(reference, HTTP_WHITEBOARD_SERVLET_ASYNC_SUPPORTED);
        String[] patterns = getStringCollectionProperty(reference, HTTP_WHITEBOARD_SERVLET_PATTERN);
        String[] errorPages = getStringCollectionProperty(reference, HTTP_WHITEBOARD_SERVLET_ERROR_PAGE);
        if ((patterns == null || patterns.length == 0) && (errorPages == null || errorPages.length == 0)) {
            // Check whether we've got a Servlet registration from the old Felix HTTP service...
            String alias = getStringProperty(reference, FELIX_SERVLET_ALIAS);
            if (alias != null) {
                patterns = new String[] { alias };
            }
            else {
                // No alias or pattern defined, this is needed for servlets...
                return false;
            }
        }
        String name = getStringProperty(reference, HTTP_WHITEBOARD_SERVLET_NAME);
        if (name == null) {
            // See RFC-189, 5.2.4...
            name = servlet.getClass().getName();
        }

        ServletMapping mapping = new ServletMapping();
        mapping.setPathSpecs(patterns);
        mapping.setServletName(name);

        ServletHolder holder = new ServletHolder(Source.EMBEDDED);
        holder.setAsyncSupported(async);
        holder.setServlet(servlet);
        holder.setName(name);

        getServletHandler().addServlet(holder, mapping);

        return true;
    }

    /**
     * @return the {@link ServletContextHelper} instance, never <code>null</code>.
     */
    public ServletContextHelper getContextHelper() {
        return m_contextHelper;
    }

    @Override
    public HttpServletHandler getServletHandler() {
        return (HttpServletHandler) super.getServletHandler();
    }

    public void removeFilter(ServiceReference<?> reference, Filter filter) {
        String name = getStringProperty(reference, HTTP_WHITEBOARD_FILTER_NAME);
        if (name == null) {
            // See RFC-189, 5.2.5...
            name = filter.getClass().getName();
        }

        getServletHandler().removeFilter(name);
    }

    public void removeServlet(ServiceReference<?> reference, Servlet servlet) {
        String name = getStringProperty(reference, HTTP_WHITEBOARD_SERVLET_NAME);
        if (name == null) {
            // See RFC-189, 5.2.4...
            name = servlet.getClass().getName();
        }

        getServletHandler().removeServlet(name);
    }

    @Override
    protected ServletHandler newServletHandler() {
        return new HttpServletHandler();
    }
}
