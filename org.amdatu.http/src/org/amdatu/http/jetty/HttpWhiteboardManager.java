/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.jetty;

import static org.amdatu.http.jetty.Util.*;
import static org.osgi.service.http.HttpConstants.HTTP_WHITEBOARD_CONTEXT_NAME;

import java.io.IOException;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.http.jetty.tracker.ServletContextHelperTracker;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.MultiException;
import org.eclipse.jetty.util.component.AbstractLifeCycle;
import org.eclipse.jetty.util.component.LifeCycle;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.ServletContextHelper;

/**
 * Tracks {@link ServletContextHelper}s and create an appropriate {@link HttpWhiteboardService} instances for each registered helper.
 */
public class HttpWhiteboardManager extends AbstractLifeCycle implements Handler {
    private static final Logger LOG = Log.getLogger(HttpWhiteboardManager.class);

    private final ServletContextHelperTracker m_contextHelperTracker;
    private final ConcurrentMap<ServiceReference<?>, HttpWhiteboardService> m_whiteboardServices;
    private final SortedSet<HttpWhiteboardService> m_sortedServices;

    private volatile Server m_server;

    /**
     * Creates a new {@link HttpWhiteboardManager} instance.
     */
    public HttpWhiteboardManager(BundleContext context) {
        m_contextHelperTracker = new ServletContextHelperTracker(context, this);
        m_whiteboardServices = new ConcurrentHashMap<>();
        m_sortedServices = new ConcurrentSkipListSet<>();
    }

    public void addContextHelper(ServiceReference<?> reference, ServletContextHelper contextHelper) {
        String contextId = getStringProperty(reference, HTTP_WHITEBOARD_CONTEXT_NAME);

        HttpWhiteboardService service = new HttpWhiteboardService(reference, contextId, contextHelper);

        HttpWhiteboardService oldService = m_whiteboardServices.put(reference, service);
        if (oldService != null) {
            m_sortedServices.remove(oldService);
            // TODO transfer servlets & filters?
            stopWhiteboardService(oldService);
        }
        m_sortedServices.add(service);

        if (m_server.isStarted() || m_server.isStarting()) {
            // When the server is already started, it won't automagically start our context as well...
            startWhiteboardService(service);
        }
    }

    @Override
    public void destroy() {
        // Nop
    }

    public void doStart() {
        m_contextHelperTracker.open();
    }

    public void doStop() {
        m_contextHelperTracker.close();
    }

    @Override
    public Server getServer() {
        return m_server;
    }

    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {
        if (m_sortedServices.isEmpty() || !isStarted()) {
            return;
        }

        MultiException mex = null;
        for (HttpWhiteboardService service : m_sortedServices) {
            try {
                service.handle(target, baseRequest, request, response);
                if (baseRequest.isHandled()) {
                    LOG.debug("REQUEST FOR {} HANDLED BY {} (STATUS = {})", target, service, response.getStatus());
                    break;
                }
            }
            catch (IOException e) {
                throw e;
            }
            catch (RuntimeException e) {
                throw e;
            }
            catch (Exception e) {
                if (mex == null) {
                    mex = new MultiException();
                }
                mex.add(e);
            }
        }
        if (mex != null) {
            if (mex.size() == 1) {
                throw new ServletException(mex.getThrowable(0));
            }
            else {
                throw new ServletException(mex);
            }
        }
    }

    public void removeContextHelper(ServiceReference<?> reference, ServletContextHelper contextHelper) {
        HttpWhiteboardService service = m_whiteboardServices.remove(reference);
        if (service != null) {
            m_sortedServices.remove(service);

            stopWhiteboardService(service);
        }
    }

    @Override
    public void setServer(Server server) {
        if (m_server == server) {
            return;
        }
        if (isStarted()) {
            throw new IllegalStateException(STARTED);
        }
        m_server = server;
        // TODO remove old listener in case a new server is set?!
        m_server.addLifeCycleListener(new LifeCycle.Listener() {
            @Override
            public void lifeCycleFailure(LifeCycle event, Throwable cause) {
                stopWhiteboardServices();
            }
            
            @Override
            public void lifeCycleStarted(LifeCycle event) {
                startWhiteboardServices();
            }
            
            @Override
            public void lifeCycleStarting(LifeCycle event) {
                // Nop
            }
            
            @Override
            public void lifeCycleStopped(LifeCycle event) {
                // Nop
            }
            
            @Override
            public void lifeCycleStopping(LifeCycle event) {
                stopWhiteboardServices();
            }
        });
        
        // Ensure already present whiteboard services are started...
        if (m_server.isRunning()) {
            startWhiteboardServices();
        }
    }
    
    final void startWhiteboardServices() {
        for (HttpWhiteboardService service : m_whiteboardServices.values()) {
            if (!service.isRunning() && !service.isFailed()) {
                startWhiteboardService(service);
            }
        }
    }
    
    final void stopWhiteboardServices() {
        for (HttpWhiteboardService service : m_whiteboardServices.values()) {
            if (service.isRunning()) {
                stopWhiteboardService(service);
            }
        }
    }

    private void startWhiteboardService(HttpWhiteboardService service) {
        try {
            service.setServer(m_server);

            service.start();
        }
        catch (Exception e) {
            LOG.warn("Failed to start whiteboard service!", e);
        }
    }

    private void stopWhiteboardService(HttpWhiteboardService service) {
        try {
            service.stop();

            service.setServer(null);
        }
        catch (Exception e) {
            LOG.warn("Failed to stop whiteboard service!", e);
        }
    }
}
