/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.jetty;

import java.util.Arrays;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Hashtable;

import org.eclipse.jetty.util.component.LifeCycle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * Provides generic utility methods.
 */
class Util {

    private Util() {
        // Utility class, non instantiable...
    }
    
    public static boolean shouldBeStarted(LifeCycle lifecycle) {
        return !lifecycle.isFailed() && !lifecycle.isRunning();
    }

    public static Dictionary<String, ?> createDictionary(Object... entries) {
        Dictionary<String, Object> props = new Hashtable<>();
        for (int i = 0; i < entries.length; i += 2) {
            String key = (String) entries[i];
            Object value = entries[i + 1];
            props.put(key, value);
        }
        return props;
    }

    public static boolean getBooleanProperty(BundleContext bc, String key) {
        return parseBooleanProperty(bc.getProperty(key));
    }

    public static boolean getBooleanProperty(ServiceReference ref, String key) {
        return parseBooleanProperty(ref.getProperty(key));
    }

    public static int getIntProperty(BundleContext bc, String key, int defValue) {
        return parseIntProperty(bc.getProperty(key), defValue);
    }

    public static int getIntProperty(ServiceReference ref, String key, int defValue) {
        return parseIntProperty(ref.getProperty(key), defValue);
    }

    public static String[] getStringCollectionProperty(BundleContext bc, String key) {
        return parseStringCollection(bc.getProperty(key));
    }

    public static String[] getStringCollectionProperty(ServiceReference ref, String key) {
        return parseStringCollection(ref.getProperty(key));
    }

    public static String getStringProperty(BundleContext bc, String key) {
        Object value = bc.getProperty(key);
        return (value instanceof String) ? (String) value : null;
    }

    public static String getStringProperty(ServiceReference ref, String key) {
        Object value = ref.getProperty(key);
        return (value instanceof String) ? (String) value : null;
    }

    public static boolean isEmpty(final String value) {
        return value == null || "".equals(value.trim());
    }

    public static boolean parseBooleanProperty(Object value) {
        if (value instanceof String) {
            return Boolean.valueOf((String) value);
        }
        else if (value instanceof Boolean) {
            return ((Boolean) value).booleanValue();
        }
        return false;
    }

    public static int parseIntProperty(Object value, int defValue) {
        if (value == null) {
            return defValue;
        }

        try {
            return Integer.parseInt(value.toString());
        }
        catch (Exception e) {
            return defValue;
        }
    }

    public static String[] parseStringCollection(Object value) {
        if (value instanceof String) {
            return new String[] { (String) value };
        }
        else if (value instanceof String[]) {
            return (String[]) value;
        }
        else if (value instanceof Collection) {
            Collection coll = (Collection) value;
            return (String[]) coll.toArray(new String[coll.size()]);
        }
        return null;
    }

    public static <T> T[] removeElementAt(T[] array, int idx) {
        if (idx < 0) {
            return array;
        }
        T[] newArray = Arrays.copyOf(array, array.length - 1);
        if (idx > 0) {
            System.arraycopy(array, 0, newArray, 0, idx);
        }
        if (idx + 1 < array.length) {
            System.arraycopy(array, idx + 1, newArray, idx, array.length - (idx + 1));
        }
        return newArray;
    }
}
