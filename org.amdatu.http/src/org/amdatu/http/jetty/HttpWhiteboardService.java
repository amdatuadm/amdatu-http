/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.jetty;

import static org.amdatu.http.jetty.Util.getStringCollectionProperty;
import static org.amdatu.http.jetty.Util.isEmpty;
import static org.osgi.service.http.HttpConstants.HTTP_WHITEBOARD_SERVLET_ERROR_PAGE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.Iterator;
import java.util.List;

import javax.servlet.AsyncListener;
import javax.servlet.Filter;
import javax.servlet.Servlet;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.ServletRequestAttributeListener;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionListener;

import org.amdatu.http.jetty.tracker.EventListenerTracker;
import org.amdatu.http.jetty.tracker.FilterTracker;
import org.amdatu.http.jetty.tracker.ServletTracker;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.component.LifeCycle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpConstants;
import org.osgi.service.http.ServletContextHelper;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Represents a whiteboard-service for a particular (named) servlet context helper.
 */
public class HttpWhiteboardService implements Comparable<HttpWhiteboardService>, LifeCycle {
    private final ServiceReference<?> m_serviceRef;
    private final HttpErrorHandler m_errorHandler;
    private final HttpServletContextHandler m_contextHandler;
    private final String m_contextId;
    private final String m_filter;
    private final List<ServiceTracker> m_trackers;

    public HttpWhiteboardService(ServiceReference<?> serviceRef) {
        this(serviceRef, null, new DefaultServletContextHelper(serviceRef.getBundle()));
    }

    public HttpWhiteboardService(ServiceReference<?> serviceRef, String contextId, ServletContextHelper contextHelper) {
        m_serviceRef = serviceRef;
        m_contextId = isEmpty(contextId) ? null : contextId;
        m_contextHandler = new HttpServletContextHandler(contextHelper);

        m_errorHandler = new HttpErrorHandler();
        m_contextHandler.setErrorHandler(m_errorHandler);

        if (m_contextId == null) {
            m_filter = String.format("(!(%s=*))", HttpConstants.HTTP_WHITEBOARD_CONTEXT_SELECT);
        }
        else {
            m_filter = String.format("(%s=%s)", HttpConstants.HTTP_WHITEBOARD_CONTEXT_SELECT, m_contextId);
        }

        m_trackers = new ArrayList<>();
    }

    public boolean addEventListener(ServiceReference<?> reference, EventListener listener) {
        if (listener instanceof ServletContextListener || listener instanceof ServletContextAttributeListener
            || listener instanceof ServletRequestListener || listener instanceof ServletRequestAttributeListener) {
            m_contextHandler.addEventListener(listener);

            // See RFC-189, 5.2.7.1...
            if ((listener instanceof ServletContextListener) && m_contextHandler.isStarted()) {
                ServletContextEvent event = new ServletContextEvent(m_contextHandler.getServletContext());
                m_contextHandler.callContextInitialized((ServletContextListener) listener, event);
            }
        }
        else if (listener instanceof HttpSessionListener || listener instanceof HttpSessionAttributeListener) {
            m_contextHandler.getSessionHandler().addEventListener(listener);
        }
        else if (listener instanceof AsyncListener) {
            // TODO add tests for async filters/servlets...
        }

        return true;
    }

    public boolean addFilter(ServiceReference<?> reference, Filter filter) {
        return m_contextHandler.addFilter(reference, filter);
    }

    @Override
    public void addLifeCycleListener(Listener listener) {
        throw new UnsupportedOperationException();
    }

    public boolean addServlet(ServiceReference<?> reference, Servlet servlet) {
        String[] errorPages = getStringCollectionProperty(reference, HTTP_WHITEBOARD_SERVLET_ERROR_PAGE);
        if (errorPages != null && errorPages.length > 0) {
            // Add the given servlet to the list of possible error handlers...
            m_errorHandler.addErrorServlet(reference, servlet, errorPages);
        }
        return m_contextHandler.addServlet(reference, servlet);
    }

    /**
     * Compares this service's contextId with the contextId of a given {@link HttpWhiteboardService}, ensuring that <code>null</code>-values are always regarded less than non-<code>null</code> values.
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(HttpWhiteboardService other) {
        int result = 0;
        if (m_contextId == null) {
            if (other.m_contextId != null) {
                result = 1;
            }
        }
        else {
            if (other.m_contextId == null) {
                result = -1;
            }
        }
        if (result == 0) {
            result = m_serviceRef.compareTo(other.m_serviceRef);
        }
        return result;
    }

    /**
     * @return the context ID for which this service is listening, can be <code>null</code> in case of the default service.
     */
    public String getContextId() {
        return m_contextId;
    }

    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {
        m_contextHandler.handle(target, baseRequest, request, response);
    }

    @Override
    public boolean isFailed() {
        return m_contextHandler.isFailed();
    }

    @Override
    public boolean isRunning() {
        return m_contextHandler.isRunning();
    }

    @Override
    public boolean isStarted() {
        return m_contextHandler.isStarted();
    }

    @Override
    public boolean isStarting() {
        return m_contextHandler.isStarting();
    }

    @Override
    public boolean isStopped() {
        return m_contextHandler.isStopped();
    }

    @Override
    public boolean isStopping() {
        return m_contextHandler.isStopping();
    }

    public void removeEventListener(ServiceReference<?> reference, EventListener listener) {
        if (listener instanceof ServletContextListener || listener instanceof ServletContextAttributeListener
            || listener instanceof ServletRequestListener || listener instanceof ServletRequestAttributeListener) {
            m_contextHandler.removeEventListener(listener);
        }
        else if (listener instanceof HttpSessionListener || listener instanceof HttpSessionAttributeListener) {
            m_contextHandler.getSessionHandler().removeEventListener(listener);
        }
        else if (listener instanceof AsyncListener) {
            // TODO add tests for async filters/servlets...
        }
    }

    public void removeFilter(ServiceReference<?> reference, Filter filter) {
        m_contextHandler.removeFilter(reference, filter);
    }

    @Override
    public void removeLifeCycleListener(Listener listener) {
        throw new UnsupportedOperationException();
    }

    public void removeServlet(ServiceReference<?> reference, Servlet servlet) {
        String[] errorPages = getStringCollectionProperty(reference, HTTP_WHITEBOARD_SERVLET_ERROR_PAGE);
        if (errorPages != null && errorPages.length > 0) {
            // Add the given servlet to the list of possible error handlers...
            m_errorHandler.removeErrorServlet(reference, servlet, errorPages);
        }
        m_contextHandler.removeServlet(reference, servlet);
    }

    public void setServer(Server server) {
        m_contextHandler.setServer(server);
    }

    public void start() throws Exception {
        BundleContext bc = m_serviceRef.getBundle().getBundleContext();
        // Start tracking all HTTP-related services...
        addServiceTracker(new ServletTracker(bc, m_filter, this)).open();
        addServiceTracker(new FilterTracker(bc, m_filter, this)).open();
        addServiceTracker(new EventListenerTracker(bc, m_filter, this)).open();
        // Finally, start the context handler itself...
        m_contextHandler.start();
    }

    public void stop() throws Exception {
        // Stop the context handler (no more handling of requests)...
        m_contextHandler.stop();

        Iterator<ServiceTracker> iter = m_trackers.iterator();
        while (iter.hasNext()) {
            iter.next().close();
            iter.remove();
        }
        // Lastly, destroy the context handler...
        m_contextHandler.destroy();
    }

    @Override
    public String toString() {
        return String.format("HttpWhiteboardService[%s]", m_contextId);
    }

    private ServiceTracker addServiceTracker(ServiceTracker tracker) {
        m_trackers.add(tracker);
        return tracker;
    }
}
