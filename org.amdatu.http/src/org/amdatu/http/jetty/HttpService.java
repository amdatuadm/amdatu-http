/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.jetty;

import static org.amdatu.http.AmdatuHttpConstants.*;
import static org.amdatu.http.jetty.Util.parseIntProperty;
import static org.amdatu.http.jetty.Util.parseStringCollection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.amdatu.http.HttpConnectorProvider;
import org.amdatu.http.jetty.tracker.HttpConnectorProviderTracker;
import org.eclipse.jetty.server.ConnectionFactory;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.http.ServletContextHelper;

/**
 * Provides the actual HTTP service.
 */
public class HttpService implements ManagedServiceFactory {
    private static final Logger LOG = Log.getLogger(HttpService.class);

    private final ConcurrentMap<String, Connector> m_connectors;

    private HttpConnectorProviderTracker m_connProviderTracker;
    private HttpWhiteboardManager m_whiteboardManager;
    private Server m_server;

    /**
     * Creates a new {@link HttpService} instance.
     * 
     * @param context the OSGi bundle context to use, cannot be <code>null</code>.
     */
    public HttpService(BundleContext context) {
        m_connectors = new ConcurrentHashMap<>(1);

        m_connProviderTracker = new HttpConnectorProviderTracker(context);
        m_whiteboardManager = new HttpWhiteboardManager(context);

        m_server = new Server();
        m_server.setStopAtShutdown(true);
        m_server.setHandler(m_whiteboardManager);

        // Register a default ServletContextHelper for servlets without any particular bindings...
        context.registerService(ServletContextHelper.class.getName(), new ServiceFactory<ServletContextHelper>() {
            @Override
            public ServletContextHelper getService(Bundle bundle, ServiceRegistration<ServletContextHelper> registration) {
                return new DefaultServletContextHelper(bundle);
            }

            @Override
            public void ungetService(Bundle bundle, ServiceRegistration<ServletContextHelper> registration,
                ServletContextHelper service) {
                // Nop
            }
        }, null);
    }

    @Override
    public void deleted(String pid) {
        LOG.debug("Deleting configuration with PID {}", pid);

        Connector conn = m_connectors.remove(pid);
        if (conn != null) {
            m_server.removeConnector(conn);

            try {
                if (!conn.isStopping() && !conn.isStopped()) {
                    conn.stop();
                }
            }
            catch (Exception e) {
                LOG.warn("Failed to stop connector!", e);
            }
        }
    }

    @Override
    public String getName() {
        return "Amdatu-HTTP-service";
    }

    public void start() throws Exception {
        m_whiteboardManager.start();
        m_connProviderTracker.open();
    }

    public void stop() throws Exception {
        if (m_connProviderTracker != null) {
            m_connProviderTracker.close();
            m_connProviderTracker = null;
        }
        if (m_whiteboardManager != null) {
            m_whiteboardManager.stop();
            m_whiteboardManager = null;
        }
        if (m_server != null) {
            m_server.stop();
            m_server.join();
            m_server = null;
        }
    }

    @Override
    public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
        LOG.debug("Updating configuration with PID {}", pid);

        int port = parseIntProperty(properties.get(CONFIG_PORT), -1);
        if (port < 0) {
            throw new ConfigurationException(CONFIG_PORT, "invalid port number!");
        }

        String[] _types = parseStringCollection(properties.get(CONFIG_TYPES));
        if (_types == null || _types.length == 0) {
            _types = new String[] { TYPE_HTTP };
        }

        Set<String> types = new HashSet<>(Arrays.asList(_types));

        // Generic configuration (shared between HTTP/HTTPS)...
        HttpConfiguration config = new HttpConfiguration();

        Object value;
        HttpConnectionFactory http = null;

        List<ConnectionFactory> factories = new ArrayList<>();
        if (types.contains(TYPE_HTTP)) {
            http = new HttpConnectionFactory(config);
            factories.add(http);
        }
        if (types.contains(TYPE_HTTPS)) {
            HttpConfiguration secureConfig = new HttpConfiguration(config);
            secureConfig.addCustomizer(new SecureRequestCustomizer());

            if (http == null) {
                http = new HttpConnectionFactory(secureConfig);
                factories.add(http);
            }

            SslContextFactory factory = new SslContextFactory();
            factory.setKeyStorePath((String) properties.get(CONFIG_KEYSTORE_PATH));
            factory.setKeyStorePassword((String) properties.get(CONFIG_KEYSTORE_STORE_PWD));
            factory.setKeyManagerPassword((String) properties.get(CONFIG_KEYSTORE_KEY_PWD));
            value = properties.get(CONFIG_KEYSTORE_INCLUDE_PROTOCOLS);
            if (value != null) {
                factory.setIncludeProtocols(parseStringCollection(value));
            }
            value = properties.get(CONFIG_KEYSTORE_PROTOCOL);
            if (value != null) {
                factory.setProtocol((String) value);
            }

            SslConnectionFactory https = new SslConnectionFactory(factory, http.getProtocol());
            factories.add(0, https);
        }

        for (HttpConnectorProvider provider : m_connProviderTracker.getProviders()) {
            provider.handle(types, properties, factories);
        }
        
        if (factories.isEmpty()) {
            throw new ConfigurationException(CONFIG_TYPES, "No connection factories defined?");
        }

        ServerConnector conn = new ServerConnector(m_server, factories.toArray(new ConnectionFactory[factories.size()]));
        conn.setPort(port);

        String host = (String) properties.get(CONFIG_HOST);
        if (host != null) {
            conn.setHost(host);
        }

        if (m_connectors.putIfAbsent(pid, conn) == null) {
            m_server.addConnector(conn);

            try {
                if (!m_server.isRunning() && !m_server.isFailed()) {
                    m_server.start();
                }
                if (!conn.isRunning() && !conn.isFailed()) {
                    // see https://bugs.eclipse.org/bugs/show_bug.cgi?id=426897
                    conn.start();
                }
            }
            catch (Exception e) {
                LOG.warn("Failed to start server or connector!", e);
                throw new ConfigurationException(null, "Failed to start server or connector!", e);
            }
        }
        else {
            throw new ConfigurationException(null, "Cannot update connector settings for pid: " + pid);
        }
    }
}
