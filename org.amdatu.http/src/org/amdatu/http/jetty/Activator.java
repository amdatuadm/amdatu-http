/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.jetty;

import static org.amdatu.http.jetty.Util.createDictionary;
import static org.amdatu.http.jetty.Util.getBooleanProperty;

import org.amdatu.http.AmdatuHttpConstants;
import org.eclipse.jetty.util.log.Log;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedServiceFactory;

/**
 * Provides the entry point for this bundle.
 */
public class Activator implements BundleActivator {
    private static final String PID = AmdatuHttpConstants.FACTORY_PID;

    // Locally managed...
    private HttpLogger m_logger;
    private HttpService m_httpService;

    @Override
    public void start(BundleContext context) throws Exception {
        m_logger = new HttpLogger(context);
        m_logger.setDebugEnabled(getBooleanProperty(context, "org.amdatu.http.debug"));
        // Use our own log instance...
        Log.setLog(m_logger);

        m_logger.open();

        m_httpService = new HttpService(context);
        m_httpService.start();

        context.registerService(ManagedServiceFactory.class.getName(), m_httpService, createDictionary(Constants.SERVICE_PID, PID));
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        if (m_httpService != null) {
            m_httpService.stop();
            m_httpService = null;
        }
        if (m_logger != null) {
            m_logger.close();
            m_logger = null;
        }
    }
}
