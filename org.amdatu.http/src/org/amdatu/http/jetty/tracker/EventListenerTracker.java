/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.jetty.tracker;

import java.util.EventListener;

import org.amdatu.http.jetty.HttpWhiteboardService;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

/**
 * Generic tracker for the various {@link EventListener} subtypes defined in <tt>javax.servlet</tt>.
 */
public class EventListenerTracker extends AbstractTracker<EventListener> {

    public EventListenerTracker(BundleContext context, String filter, HttpWhiteboardService whiteboardService) throws InvalidSyntaxException {
        super(context, "(&" + filter + "(|(objectClass=javax.servlet.AsyncListener)"
            + "(objectClass=javax.servlet.ServletContextAttributeListener)"
            + "(objectClass=javax.servlet.ServletContextListener)"
            + "(objectClass=javax.servlet.ServletRequestAttributeListener)"
            + "(objectClass=javax.servlet.ServletRequestListener)"
            + "(objectClass=javax.servlet.http.HttpSessionAttributeListener)"
            + "(objectClass=javax.servlet.http.HttpSessionListener)))", whiteboardService);
    }

    @Override
    protected boolean added(ServiceReference reference, EventListener service) {
        return m_whiteboardService.addEventListener(reference, service);
    }

    @Override
    protected void removed(ServiceReference reference, EventListener service) {
        m_whiteboardService.removeEventListener(reference, service);
    }
}
