/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.jetty.tracker;

import java.util.Arrays;

import org.amdatu.http.HttpConnectorProvider;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Tracks all registered {@link HttpConnectorProvider} instances.
 */
public class HttpConnectorProviderTracker extends ServiceTracker<HttpConnectorProvider, HttpConnectorProvider> {

    public HttpConnectorProviderTracker(BundleContext context) {
        super(context, HttpConnectorProvider.class, null);
    }

    /**
     * @return an array of available {@link HttpConnectorProvider}s, never <code>null</code>.
     */
    public HttpConnectorProvider[] getProviders() {
        Object[] services = super.getServices();
        if (services == null) {
            return new HttpConnectorProvider[0];
        }
        return Arrays.copyOf(services, services.length, HttpConnectorProvider[].class);
    }
}
