/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.jetty.tracker;

import org.amdatu.http.jetty.HttpWhiteboardService;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Abstract service tracker for various services.
 */
@SuppressWarnings("unchecked")
abstract class AbstractTracker<T> extends ServiceTracker<T, T> {
    protected final HttpWhiteboardService m_whiteboardService;

    protected AbstractTracker(BundleContext context, Class<T> type, String extraFilter, HttpWhiteboardService whiteboardService) throws InvalidSyntaxException {
        this(context, String.format("(&(objectClass=%s)%s)", type.getName(), extraFilter), whiteboardService);
    }

    protected AbstractTracker(BundleContext context, String filter, HttpWhiteboardService whiteboardService) throws InvalidSyntaxException {
        super(context, FrameworkUtil.createFilter(filter), null);

        m_whiteboardService = whiteboardService;
    }

    @Override
    public final T addingService(ServiceReference<T> reference) {
        T service = (T) context.getService(reference);
        if (added(reference, service)) {
            return service;
        }
        else {
            context.ungetService(reference);
        }
        return null;
    }

    @Override
    public final void modifiedService(ServiceReference reference, T service) {
        modified(reference, (T) service);
    }

    @Override
    public final void removedService(ServiceReference reference, T service) {
        removed(reference, (T) service);
        context.ungetService(reference);
    }

    protected abstract boolean added(ServiceReference reference, T service);

    protected void modified(ServiceReference reference, T service) {
        added(reference, service);
        removed(reference, service);
    }

    protected abstract void removed(ServiceReference reference, T service);

}
