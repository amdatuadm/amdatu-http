/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.jetty.tracker;

import javax.servlet.Filter;

import org.amdatu.http.jetty.HttpWhiteboardService;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

/**
 * Tracks all {@link Filter}s that are registered whiteboard-style.
 */
public class FilterTracker extends AbstractTracker<Filter> {

    public FilterTracker(BundleContext context, String filter, HttpWhiteboardService whiteboardService) throws InvalidSyntaxException {
        super(context, Filter.class, filter, whiteboardService);
    }

    @Override
    protected boolean added(ServiceReference reference, Filter service) {
        return m_whiteboardService.addFilter(reference, service);
    }

    @Override
    protected void removed(ServiceReference reference, Filter service) {
        m_whiteboardService.removeFilter(reference, service);
    }
}
