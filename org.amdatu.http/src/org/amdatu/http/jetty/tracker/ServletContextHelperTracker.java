/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.jetty.tracker;

import org.amdatu.http.jetty.HttpWhiteboardManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.ServletContextHelper;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Tracks all {@link ServletContextHelper}s that are registered whiteboard-style.
 */
public class ServletContextHelperTracker extends ServiceTracker<ServletContextHelper, ServletContextHelper> {
    private final HttpWhiteboardManager m_manager;

    public ServletContextHelperTracker(BundleContext context, HttpWhiteboardManager manager) {
        super(context, ServletContextHelper.class, null);

        m_manager = manager;
    }

    @Override
    public ServletContextHelper addingService(ServiceReference<ServletContextHelper> reference) {
        ServletContextHelper service = super.addingService(reference);
        m_manager.addContextHelper(reference, service);
        return service;
    }

    @Override
    public void removedService(ServiceReference<ServletContextHelper> reference, ServletContextHelper service) {
        m_manager.removeContextHelper(reference, service);
        super.removedService(reference, service);
    }
}
