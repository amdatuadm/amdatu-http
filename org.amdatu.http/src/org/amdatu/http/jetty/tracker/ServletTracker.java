/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.jetty.tracker;

import javax.servlet.Servlet;

import org.amdatu.http.jetty.HttpWhiteboardService;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

/**
 * Tracks all {@link Servlet}s that are registered whiteboard-style.
 */
public class ServletTracker extends AbstractTracker<Servlet> {

    public ServletTracker(BundleContext context, String filter, HttpWhiteboardService whiteboardService) throws InvalidSyntaxException {
        super(context, Servlet.class, filter, whiteboardService);
    }

    @Override
    protected boolean added(ServiceReference reference, Servlet service) {
        return m_whiteboardService.addServlet(reference, service);
    }

    @Override
    protected void removed(ServiceReference reference, Servlet service) {
        m_whiteboardService.removeServlet(reference, service);
    }
}
