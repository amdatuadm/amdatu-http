/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.itest.core;

import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

import java.io.IOException;
import java.net.URL;
import java.util.Hashtable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.http.itest.AbstractHttpTest;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.http.HttpConstants;

/**
 * Test cases for the Amdatu HTTP service.
 */
public class HttpErrorPageTest extends AbstractHttpTest {

    public void testCustomErrorHandlerOk() throws Exception {
        final CountDownLatch initLatch = new CountDownLatch(2);
        final CountDownLatch testDestroyLatch = new CountDownLatch(1);
        final CountDownLatch errorDestroyLatch = new CountDownLatch(1);

        Servlet testServlet = new TestServlet(initLatch, testDestroyLatch) {
            @Override
            protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                String error = req.getParameter("error");
                if ("500".equals(error)) {
                    resp.sendError(500);
                }
                else if ("IO".equals(error)) {
                    throw new IOException("IOException");
                }
            }
        };

        Servlet errorHandlerServlet = new TestServlet(initLatch, errorDestroyLatch) {
            @Override
            protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                String error = req.getParameter("error");
                resp.setContentType("text/plain");
                resp.getOutputStream().write(("error: " + error).getBytes());
            }
        };

        URL testURL;
        Hashtable<String, Object> props;

        props = new Hashtable<>();
        props.put(HttpConstants.HTTP_WHITEBOARD_SERVLET_PATTERN, "/test");

        ServiceRegistration serviceReg1 = registerServlet(testServlet, props);

        props.clear();
        props.put(HttpConstants.HTTP_WHITEBOARD_SERVLET_ERROR_PAGE, new String[] { "500", "java.io.IOException" });

        ServiceRegistration serviceReg2 = registerServlet(errorHandlerServlet, props);

        assertTrue("Servlet not intitialized?!", initLatch.await(1, TimeUnit.SECONDS));

        testURL = new URL("http://localhost:8080/test?error=500");

        assertContent("error: 500", testURL);

        testURL = new URL("http://localhost:8080/test?error=IO");

        assertContent("error: IO", testURL);

        // Remove the error-handling servlet as service...
        serviceReg2.unregister();

        assertTrue("Error servlet not destroyed?!", errorDestroyLatch.await(1, TimeUnit.SECONDS));

        assertResponseCode(HTTP_INTERNAL_ERROR, testURL);

        testURL = new URL("http://localhost:8080/test?error=500");

        assertResponseCode(HTTP_INTERNAL_ERROR, testURL);

        serviceReg1.unregister();

        assertTrue("Test servlet not destroyed?!", testDestroyLatch.await(1, TimeUnit.SECONDS));

        assertResponseCode(HTTP_NOT_FOUND, testURL);
    }
}
