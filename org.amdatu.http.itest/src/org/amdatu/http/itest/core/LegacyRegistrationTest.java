/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.itest.core;

import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

import java.io.IOException;
import java.net.URL;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.http.itest.AbstractHttpTest;
import org.osgi.framework.ServiceRegistration;

/**
 * Test cases for the registration of servlets/filters using the legacy method as used by Felix HTTP.
 */
public class LegacyRegistrationTest extends AbstractHttpTest {

    public void testAddRemoveLegacyFilterOk() throws Exception {
        final CountDownLatch initLatch = new CountDownLatch(1);
        final CountDownLatch destroyLatch = new CountDownLatch(1);

        Filter testFilter = new TestFilter(initLatch, destroyLatch) {
            @Override
            public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException,
                ServletException {
                resp.setContentType("text/plain");
                resp.getOutputStream().write(MESSAGE.getBytes());
            }
        };

        Dictionary<String, Object> props = new Hashtable<>();
        props.put("pattern", "/test");

        ServiceRegistration serviceReg = registerFilter(testFilter, props);

        assertTrue("Filter not intitialized?!", initLatch.await(1, TimeUnit.SECONDS));

        URL testURL = new URL("http://localhost:8080/test");

        assertContent(MESSAGE, testURL);

        // Remove the servlet as service...
        serviceReg.unregister();

        assertTrue("Filter not destroyed?!", destroyLatch.await(1, TimeUnit.SECONDS));

        assertResponseCode(HTTP_NOT_FOUND, testURL);
    }

    public void testAddRemoveLegacyServletOk() throws Exception {
        final CountDownLatch initLatch = new CountDownLatch(1);
        final CountDownLatch destroyLatch = new CountDownLatch(1);

        Servlet testServlet = new TestServlet(initLatch, destroyLatch) {
            @Override
            protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                resp.setContentType("text/plain");
                resp.getOutputStream().write(MESSAGE.getBytes());
            }
        };

        Dictionary<String, Object> props = new Hashtable<>();
        props.put("alias", "/test");

        ServiceRegistration serviceReg = registerServlet(testServlet, props);

        assertTrue("Servlet not intitialized?!", initLatch.await(1, TimeUnit.SECONDS));

        URL testURL = new URL("http://localhost:8080/test");

        assertContent(MESSAGE, testURL);

        // Remove the servlet as service...
        serviceReg.unregister();

        assertTrue("Servlet not destroyed?!", destroyLatch.await(1, TimeUnit.SECONDS));

        assertResponseCode(HTTP_NOT_FOUND, testURL);
    }

}
