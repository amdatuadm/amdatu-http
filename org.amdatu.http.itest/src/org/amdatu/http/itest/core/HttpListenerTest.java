/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.itest.core;

import java.util.Hashtable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.servlet.Servlet;
import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.amdatu.http.itest.AbstractHttpTest;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.http.HttpConstants;
import org.osgi.service.http.ServletContextHelper;

/**
 * Test cases for the Amdatu HTTP service.
 */
public class HttpListenerTest extends AbstractHttpTest {

    public void testServletContextAttributeListenerForDefaultContextOk() throws Exception {
        final CountDownLatch addLatch = new CountDownLatch(2);
        final CountDownLatch removeLatch = new CountDownLatch(1);

        final String attrKey = "key";
        final String attrValue1 = "value1";
        final String attrValue2 = "value2";

        ServletContextAttributeListener listener = new ServletContextAttributeListener() {
            @Override
            public void attributeAdded(ServletContextAttributeEvent event) {
                if (attrKey.equals(event.getName()) && attrValue1.equals(event.getValue())
                    && attrValue1.equals(event.getServletContext().getAttribute(attrKey))) {
                    addLatch.countDown();
                }
            }

            @Override
            public void attributeRemoved(ServletContextAttributeEvent event) {
                if (attrKey.equals(event.getName())) {
                    removeLatch.countDown();
                }
            }

            @Override
            public void attributeReplaced(ServletContextAttributeEvent event) {
                if (attrKey.equals(event.getName()) && attrValue1.equals(event.getValue())
                    && attrValue2.equals(event.getServletContext().getAttribute(attrKey))) {
                    addLatch.countDown();
                }
            }
        };

        Servlet servlet = new HttpServlet() {
            @Override
            public void destroy() {
                getServletContext().removeAttribute(attrKey);
            }

            @Override
            public void init() throws ServletException {
                getServletContext().setAttribute(attrKey, attrValue1);

                getServletContext().setAttribute(attrKey, attrValue2);
            }
        };

        Hashtable<String, Object> props = new Hashtable<>();

        // Register listener first...
        registerEventListener(ServletContextAttributeListener.class, listener, props);

        props.clear();
        props.put(HttpConstants.HTTP_WHITEBOARD_SERVLET_PATTERN, "/test");

        ServiceRegistration serviceReg = registerServlet(servlet, props);

        assertTrue("Listener not called?!", addLatch.await(1, TimeUnit.SECONDS));

        serviceReg.unregister();

        assertTrue("Listener not called?!", removeLatch.await(1, TimeUnit.SECONDS));
    }

    public void testServletContextAttributeListenerOk() throws Exception {
        final CountDownLatch addLatch = new CountDownLatch(2);
        final CountDownLatch removeLatch = new CountDownLatch(1);

        final String contextName = createUniqueContextName();

        final String attrKey = "key";
        final String attrValue1 = "value1";
        final String attrValue2 = "value2";

        ServletContextAttributeListener listener = new ServletContextAttributeListener() {
            @Override
            public void attributeAdded(ServletContextAttributeEvent event) {
                if (attrKey.equals(event.getName()) && attrValue1.equals(event.getValue())
                    && attrValue1.equals(event.getServletContext().getAttribute(attrKey))) {
                    addLatch.countDown();
                }
            }

            @Override
            public void attributeRemoved(ServletContextAttributeEvent event) {
                if (attrKey.equals(event.getName())) {
                    removeLatch.countDown();
                }
            }

            @Override
            public void attributeReplaced(ServletContextAttributeEvent event) {
                if (attrKey.equals(event.getName()) && attrValue1.equals(event.getValue())
                    && attrValue2.equals(event.getServletContext().getAttribute(attrKey))) {
                    addLatch.countDown();
                }
            }
        };

        ServletContextHelper helper = new ServletContextHelper() {
            // Nop...
        };

        Servlet servlet = new HttpServlet() {
            @Override
            public void destroy() {
                getServletContext().removeAttribute(attrKey);
            }

            @Override
            public void init() throws ServletException {
                getServletContext().setAttribute(attrKey, attrValue1);

                getServletContext().setAttribute(attrKey, attrValue2);
            }
        };

        Hashtable<String, Object> props = new Hashtable<>();
        props.put(HttpConstants.HTTP_WHITEBOARD_CONTEXT_SELECT, contextName);

        // Register listener first...
        registerEventListener(ServletContextAttributeListener.class, listener, props);

        props.clear();
        props.put(HttpConstants.HTTP_WHITEBOARD_CONTEXT_NAME, contextName);

        registerServletContextHelper(helper, props);

        props.clear();
        props.put(HttpConstants.HTTP_WHITEBOARD_CONTEXT_SELECT, contextName);
        props.put(HttpConstants.HTTP_WHITEBOARD_SERVLET_PATTERN, "/test");

        ServiceRegistration serviceReg = registerServlet(servlet, props);

        assertTrue("Listener not called?!", addLatch.await(1, TimeUnit.SECONDS));

        serviceReg.unregister();

        assertTrue("Listener not called?!", removeLatch.await(1, TimeUnit.SECONDS));
    }

    public void testServletContextListenerForDefaultContextOk() throws Exception {
        final CountDownLatch initLatch = new CountDownLatch(1);
        final CountDownLatch destroyLatch = new CountDownLatch(1);

        ServletContextListener listener = new ServletContextListener() {
            @Override
            public void contextDestroyed(ServletContextEvent event) {
                destroyLatch.countDown();
            }

            @Override
            public void contextInitialized(ServletContextEvent event) {
                initLatch.countDown();
            }
        };

        Hashtable<String, Object> props = new Hashtable<>();

        // Register listener first...
        registerEventListener(ServletContextListener.class, listener, props);

        assertTrue("Listener not called?!", initLatch.await(1, TimeUnit.SECONDS));

        // Stop the HTTP service, should destroy our default context...
        Bundle httpBundle = getHttpBundle();
        httpBundle.stop();

        try {
            assertTrue("Listener not called?!", destroyLatch.await(1, TimeUnit.SECONDS));
        }
        finally {
            // Make sure to start the bundle again, otherwise succeeding tests might fail!
            httpBundle.start();
        }
    }

    public void testServletContextListenerOk() throws Exception {
        final CountDownLatch initLatch = new CountDownLatch(1);
        final CountDownLatch destroyLatch = new CountDownLatch(1);

        final String contextName = createUniqueContextName();

        ServletContextListener listener = new ServletContextListener() {
            @Override
            public void contextDestroyed(ServletContextEvent event) {
                destroyLatch.countDown();
            }

            @Override
            public void contextInitialized(ServletContextEvent event) {
                initLatch.countDown();
            }
        };

        ServletContextHelper helper = new ServletContextHelper() {
            // Nop...
        };

        Hashtable<String, Object> props = new Hashtable<>();
        props.put(HttpConstants.HTTP_WHITEBOARD_CONTEXT_SELECT, contextName);

        // Register listener first...
        registerEventListener(ServletContextListener.class, listener, props);

        props.clear();
        props.put(HttpConstants.HTTP_WHITEBOARD_CONTEXT_NAME, contextName);

        ServiceRegistration serviceReg = registerServletContextHelper(helper, props);

        assertTrue("Listener not called?!", initLatch.await(1, TimeUnit.SECONDS));

        serviceReg.unregister();

        assertTrue("Listener not called?!", destroyLatch.await(1, TimeUnit.SECONDS));
    }
}
