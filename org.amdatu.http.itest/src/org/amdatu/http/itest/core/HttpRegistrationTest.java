/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.itest.core;

import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

import java.io.IOException;
import java.net.URL;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.http.itest.AbstractHttpTest;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.http.HttpConstants;
import org.osgi.service.http.ServletContextHelper;

/**
 * Test cases for the Amdatu HTTP service.
 */
public class HttpRegistrationTest extends AbstractHttpTest {

    public void testAddRemoveFilterDefaultContextOk() throws Exception {
        final CountDownLatch initLatch = new CountDownLatch(1);
        final CountDownLatch destroyLatch = new CountDownLatch(1);

        Filter testFilter = new TestFilter(initLatch, destroyLatch) {
            @Override
            public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
                resp.setContentType("text/plain");
                resp.getOutputStream().write(MESSAGE.getBytes());
            }
        };

        Dictionary<String, Object> props = new Hashtable<>();
        props.put(HttpConstants.HTTP_WHITEBOARD_FILTER_PATTERN, "/test");

        ServiceRegistration serviceReg = registerFilter(testFilter, props);

        assertTrue("Filter not intitialized?!", initLatch.await(1, TimeUnit.SECONDS));

        URL testURL = new URL("http://localhost:8080/test");

        assertContent(MESSAGE, testURL);

        // Remove the servlet as service...
        serviceReg.unregister();

        assertTrue("Filter not destroyed?!", destroyLatch.await(1, TimeUnit.SECONDS));

        assertResponseCode(HTTP_NOT_FOUND, testURL);
    }

    public void testAddRemoveFilterOk() throws Exception {
        final CountDownLatch initLatch = new CountDownLatch(1);
        final CountDownLatch destroyLatch = new CountDownLatch(1);

        String contextName = createUniqueContextName();

        Filter testFilter = new TestFilter(initLatch, destroyLatch) {
            @Override
            public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
                resp.setContentType("text/plain");
                resp.getOutputStream().write(MESSAGE.getBytes());
            }
        };

        Hashtable<String, Object> props = new Hashtable<>();
        props.put(HttpConstants.HTTP_WHITEBOARD_CONTEXT_SELECT, contextName);
        props.put(HttpConstants.HTTP_WHITEBOARD_FILTER_PATTERN, "/test");

        ServiceRegistration serviceReg = registerFilter(testFilter, props);

        ServletContextHelper contextHelper = new ServletContextHelper() {
        };

        props.clear();
        props.put(HttpConstants.HTTP_WHITEBOARD_CONTEXT_NAME, contextName);

        registerServletContextHelper(contextHelper, props);

        assertTrue("Filter not intitialized?!", initLatch.await(1, TimeUnit.SECONDS));

        URL testURL = new URL("http://localhost:8080/test");

        assertContent(MESSAGE, testURL);

        // Remove the servlet as service...
        serviceReg.unregister();

        assertTrue("Filter not destroyed?!", destroyLatch.await(1, TimeUnit.SECONDS));

        assertResponseCode(HTTP_NOT_FOUND, testURL);
    }

    public void testAddRemoveServletDefaultContextOk() throws Exception {
        final CountDownLatch initLatch = new CountDownLatch(1);
        final CountDownLatch destroyLatch = new CountDownLatch(1);

        Servlet testServlet = new TestServlet(initLatch, destroyLatch) {
            @Override
            protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                resp.setContentType("text/plain");
                resp.getOutputStream().write(MESSAGE.getBytes());
            }
        };

        Dictionary<String, Object> props = new Hashtable<>();
        props.put(HttpConstants.HTTP_WHITEBOARD_SERVLET_PATTERN, "/test");

        ServiceRegistration serviceReg = registerServlet(testServlet, props);

        assertTrue("Servlet not intitialized?!", initLatch.await(1, TimeUnit.SECONDS));

        URL testURL = new URL("http://localhost:8080/test");

        assertContent(MESSAGE, testURL);

        // Remove the servlet as service...
        serviceReg.unregister();

        assertTrue("Servlet not destroyed?!", destroyLatch.await(1, TimeUnit.SECONDS));

        assertResponseCode(HTTP_NOT_FOUND, testURL);
    }

    public void testAddRemoveServletOk() throws Exception {
        final CountDownLatch initLatch = new CountDownLatch(1);
        final CountDownLatch destroyLatch = new CountDownLatch(1);

        String contextName = createUniqueContextName();

        Servlet testServlet = new TestServlet(initLatch, destroyLatch) {
            @Override
            public void init(ServletConfig config) throws ServletException {
                super.init(config);
                String param = config.getInitParameter(HttpConstants.HTTP_WHITEBOARD_SERVLET_NAME);
                System.out.println("Param = " + param);
            }

            @Override
            protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                resp.setContentType("text/plain");
                resp.getOutputStream().write(MESSAGE.getBytes());
            }
        };

        Hashtable<String, Object> props = new Hashtable<>();
        props.put(HttpConstants.HTTP_WHITEBOARD_CONTEXT_SELECT, contextName);
        props.put(HttpConstants.HTTP_WHITEBOARD_SERVLET_PATTERN, "/test");

        ServiceRegistration serviceReg = registerServlet(testServlet, props);

        ServletContextHelper contextHelper = new ServletContextHelper() {
        };

        props.clear();
        props.put(HttpConstants.HTTP_WHITEBOARD_CONTEXT_NAME, contextName);

        registerServletContextHelper(contextHelper, props);

        assertTrue("Servlet not intitialized?!", initLatch.await(1, TimeUnit.SECONDS));

        URL testURL = new URL("http://localhost:8080/test");

        assertContent(MESSAGE, testURL);

        // Remove the servlet as service...
        serviceReg.unregister();

        assertTrue("Servlet not destroyed?!", destroyLatch.await(1, TimeUnit.SECONDS));

        assertResponseCode(HTTP_NOT_FOUND, testURL);
    }
}
