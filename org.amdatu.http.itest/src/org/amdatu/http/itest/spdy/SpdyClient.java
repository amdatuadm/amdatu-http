/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.itest.spdy;

import java.net.InetSocketAddress;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.spdy.api.SPDY;
import org.eclipse.jetty.spdy.api.Session;
import org.eclipse.jetty.spdy.api.Stream;
import org.eclipse.jetty.spdy.api.StreamFrameListener;
import org.eclipse.jetty.spdy.api.SynInfo;
import org.eclipse.jetty.spdy.api.server.ServerSessionFrameListener;
import org.eclipse.jetty.spdy.client.SPDYClient;
import org.eclipse.jetty.spdy.http.HTTPSPDYHeader;
import org.eclipse.jetty.util.Fields;
import org.eclipse.jetty.util.ssl.SslContextFactory;

/**
 * Very simple SPDY client.
 */
public class SpdyClient {
    private final short m_version;
    private SPDYClient.Factory m_clientFactory;

    public SpdyClient() {
        this(SPDY.V3);
    }

    public SpdyClient(short version) {
        m_version = version;

        SslContextFactory factory = new SslContextFactory();
        factory.setIncludeProtocols("TLSv1");

        m_clientFactory = new SPDYClient.Factory(factory);
        m_clientFactory.setConnectTimeout(10000);
    }

    public Session createSession(int port) throws Exception {
        return createSession(port, null);
    }

    public Session createSession(int port, ServerSessionFrameListener listener) throws Exception {
        return createSession("localhost", port, listener);
    }

    public Session createSession(String host, int port) throws Exception {
        return createSession(host, port, null);
    }

    public Session createSession(String host, int port, ServerSessionFrameListener listener) throws Exception {
        // Create one SPDYClient instance
        SPDYClient client = m_clientFactory.newSPDYClient(m_version);

        try {
            return client.connect(new InetSocketAddress(host, port), listener);
        }
        catch (CancellationException e) {
            throw new RuntimeException("Cancelled...");
        }
        catch (ExecutionException e) {
            throw (Exception) e.getCause();
        }
    }

    public Stream get(Session session, String uri) throws Exception {
        return get(session, uri, null);
    }

    public Stream get(Session session, String uri, StreamFrameListener listener) throws Exception {
        Fields headers = new Fields();
        headers.put(HTTPSPDYHeader.VERSION.name(m_version), "HTTP/1.1");
        headers.put(HTTPSPDYHeader.METHOD.name(m_version), "GET");
        headers.put(HTTPSPDYHeader.URI.name(m_version), uri);

        try {
            byte prio = 0;
            return session.syn(new SynInfo(5L, TimeUnit.SECONDS, headers, true, prio), listener);
        }
        catch (CancellationException e) {
            throw new RuntimeException("Cancelled...");
        }
        catch (ExecutionException e) {
            throw (Exception) e.getCause();
        }
    }

    public Stream post(Session session, String uri) throws Exception {
        return post(session, uri, null);
    }

    public Stream post(Session session, String uri, StreamFrameListener listener) throws Exception {
        Fields headers = new Fields();
        headers.put(HTTPSPDYHeader.VERSION.name(m_version), "HTTP/1.1");
        headers.put(HTTPSPDYHeader.METHOD.name(m_version), "POST");
        headers.put(HTTPSPDYHeader.URI.name(m_version), uri);
        headers.put("Content-Type", "application/x-www-form-urlencoded");

        try {
            byte prio = 0;
            return session.syn(new SynInfo(5L, TimeUnit.SECONDS, headers, false, prio), listener);
        }
        catch (CancellationException e) {
            throw new RuntimeException("Cancelled...");
        }
        catch (ExecutionException e) {
            throw (Exception) e.getCause();
        }
    }

    public void start() throws Exception {
        m_clientFactory.start();
    }

    public void stop() throws Exception {
        m_clientFactory.stop();
    }
}
