/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.itest.spdy;

import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

import java.io.IOException;
import java.net.URL;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.http.itest.AbstractHttpTest;
import org.eclipse.jetty.spdy.api.DataInfo;
import org.eclipse.jetty.spdy.api.SPDY;
import org.eclipse.jetty.spdy.api.Session;
import org.eclipse.jetty.spdy.api.Stream;
import org.eclipse.jetty.spdy.api.StreamFrameListener;
import org.eclipse.jetty.spdy.api.StringDataInfo;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.http.HttpConstants;

/**
 * Test cases for the Amdatu HTTP service configured to use SPDY.
 */
public class SpdyTest extends AbstractHttpTest {

    /**
     * Tests that we can talk with a SPDY-server using a SPDYv2 client.
     */
    public void testSpdyVersion2Ok() throws Exception {
        final CountDownLatch initLatch = new CountDownLatch(1);

        Servlet testServlet = new TestServlet(initLatch, null) {
            @Override
            protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                resp.setContentType("text/plain");
                resp.getOutputStream().write(MESSAGE.getBytes());
                resp.getOutputStream().flush();
            }

            @Override
            protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
                IOException {
                resp.setContentType("text/plain");
                String param = req.getParameter("param");
                resp.getOutputStream().write(param.toUpperCase(Locale.ENGLISH).getBytes());
                resp.getOutputStream().flush();
            }
        };

        Dictionary<String, Object> props = new Hashtable<>();
        props.put(HttpConstants.HTTP_WHITEBOARD_SERVLET_PATTERN, "/test");

        registerServlet(testServlet, props);

        assertTrue("Servlet not intitialized?!", initLatch.await(1, TimeUnit.SECONDS));

        SpdyClient client = new SpdyClient(SPDY.V2);
        client.start();

        try {
            final CountDownLatch getLatch = new CountDownLatch(1);

            Session session = client.createSession(8443);
            assertNotNull("No session obtained?", session);

            client.get(session, "/test", new StreamFrameListener.Adapter() {
                @Override
                public void onData(Stream stream, DataInfo dataInfo) {
                    String data = dataInfo.asString("UTF-8", true);
                    if (MESSAGE.equals(data)) {
                        getLatch.countDown();
                    }
                }
            });

            assertTrue("Get-data not received?!", getLatch.await(5, TimeUnit.SECONDS));

            final CountDownLatch postLatch = new CountDownLatch(1);

            Stream stream = client.post(session, "/test", new StreamFrameListener.Adapter() {
                @Override
                public void onData(Stream stream, DataInfo dataInfo) {
                    String data = dataInfo.asString("UTF-8", true);
                    if ("HELLO".equals(data)) {
                        postLatch.countDown();
                    }
                }
            });
            stream.data(new StringDataInfo("param=hello", true));

            assertTrue("Post-data not received?!", postLatch.await(5, TimeUnit.SECONDS));
        }
        finally {
            client.stop();
        }
    }

    /**
     * Tests that we can talk with a SPDY-server using a SPDYv3 client.
     */
    public void testSpdyVersion3Ok() throws Exception {
        final CountDownLatch initLatch = new CountDownLatch(1);

        Servlet testServlet = new TestServlet(initLatch, null) {
            @Override
            protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                resp.setContentType("text/plain");
                resp.getOutputStream().write(MESSAGE.getBytes());
                resp.getOutputStream().flush();
            }

            @Override
            protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
                IOException {
                resp.setContentType("text/plain");
                String param = req.getParameter("param");
                resp.getOutputStream().write(param.toUpperCase(Locale.ENGLISH).getBytes());
                resp.getOutputStream().flush();
            }
        };

        Dictionary<String, Object> props = new Hashtable<>();
        props.put(HttpConstants.HTTP_WHITEBOARD_SERVLET_PATTERN, "/test");

        registerServlet(testServlet, props);

        assertTrue("Servlet not intitialized?!", initLatch.await(1, TimeUnit.SECONDS));

        SpdyClient client = new SpdyClient(SPDY.V3);
        client.start();

        try {
            final CountDownLatch getLatch = new CountDownLatch(1);

            Session session = client.createSession(8443);
            assertNotNull("No session obtained?", session);

            client.get(session, "/test", new StreamFrameListener.Adapter() {
                @Override
                public void onData(Stream stream, DataInfo dataInfo) {
                    String data = dataInfo.asString("UTF-8", true);
                    if (MESSAGE.equals(data)) {
                        getLatch.countDown();
                    }
                }
            });

            assertTrue("Get-data not received?!", getLatch.await(5, TimeUnit.SECONDS));

            final CountDownLatch postLatch = new CountDownLatch(1);

            Stream stream = client.post(session, "/test", new StreamFrameListener.Adapter() {
                @Override
                public void onData(Stream stream, DataInfo dataInfo) {
                    String data = dataInfo.asString("UTF-8", true);
                    if ("HELLO".equals(data)) {
                        postLatch.countDown();
                    }
                }
            });
            stream.data(new StringDataInfo("param=hello", true));

            assertTrue("Post-data not received?!", postLatch.await(5, TimeUnit.SECONDS));
        }
        finally {
            client.stop();
        }
    }

    /**
     * Tests that clients that do not support SPDY will use the plain HTTPS version.
     */
    public void testNonSpdyClientsFallbackToHttpsOk() throws Exception {
        final CountDownLatch initLatch = new CountDownLatch(1);
        final CountDownLatch destroyLatch = new CountDownLatch(1);

        Servlet testServlet = new TestServlet(initLatch, destroyLatch) {
            @Override
            protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                resp.setContentType("text/plain");
                resp.getOutputStream().write(MESSAGE.getBytes());
                resp.getOutputStream().flush();
            }

            @Override
            protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
                IOException {
                resp.setContentType("text/plain");
                String param = req.getParameter("param");
                resp.getOutputStream().write(param.toUpperCase(Locale.ENGLISH).getBytes());
                resp.getOutputStream().flush();
            }
        };

        Dictionary<String, Object> props = new Hashtable<>();
        props.put(HttpConstants.HTTP_WHITEBOARD_SERVLET_PATTERN, "/test");

        ServiceRegistration serviceReg = registerServlet(testServlet, props);

        assertTrue("Servlet not intitialized?!", initLatch.await(1, TimeUnit.SECONDS));

        URL testURL = new URL("https://localhost:8443/test");

        assertContent(MESSAGE, testURL);

        // Remove the servlet as service...
        serviceReg.unregister();

        assertTrue("Servlet not destroyed?!", destroyLatch.await(1, TimeUnit.SECONDS));

        assertResponseCode(HTTP_NOT_FOUND, testURL);
    }

    @Override
    protected Properties getHttpConfiguration() {
        Properties props = new Properties();
        props.put("port", 8443);
        props.put("types", new String[] { "spdy" });
        props.put("keystore.path", "../etc/cert/server.jks");
        props.put("keystore.key.password", "secret");
        props.put("keystore.store.password", "secret");
        return props;
    }
}
