/*
 * Copyright (c) 2010-2013 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.itest;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.EventListener;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.Servlet;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;

import junit.framework.TestCase;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationEvent;
import org.osgi.service.cm.ConfigurationListener;
import org.osgi.service.http.ServletContextHelper;

/**
 * Test cases for the Amdatu HTTP service.
 */
public abstract class AbstractHttpTest extends TestCase {

    /**
     * {@link Filter} implementation for testing purposes.
     */
    public static class TestFilter implements Filter {
        private final CountDownLatch m_initLatch;
        private final CountDownLatch m_destroyLatch;

        public TestFilter(CountDownLatch initLatch, CountDownLatch destroyLatch) {
            m_initLatch = initLatch;
            m_destroyLatch = destroyLatch;
        }

        @Override
        public void destroy() {
            m_destroyLatch.countDown();
        }

        @Override
        public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
            // Nop
        }

        @Override
        public void init(FilterConfig config) throws ServletException {
            m_initLatch.countDown();
        }
    }

    /**
     * {@link Servlet} implementation for testing purposes.
     */
    public static class TestServlet extends HttpServlet {
        private final CountDownLatch m_initLatch;
        private final CountDownLatch m_destroyLatch;

        public TestServlet(CountDownLatch initLatch, CountDownLatch destroyLatch) {
            m_initLatch = initLatch;
            m_destroyLatch = destroyLatch;
        }

        @Override
        public void destroy() {
            if (m_destroyLatch != null) {
                m_destroyLatch.countDown();
            }
            super.destroy();
        }

        @Override
        public void init() throws ServletException {
            super.init();
            if (m_initLatch != null) {
                m_initLatch.countDown();
            }
        }
    }

    protected static final String FACTORY_PID = "org.amdatu.http.factory";

    protected static final String MESSAGE = "Hello World!";

    private static boolean m_httpAvailable = false;
    private final List<ServiceRegistration> m_managedServices = new ArrayList<>();
    private Configuration m_config;

    protected BundleContext m_context;

    protected static void assertContent(int expectedRC, String expected, URL url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        int rc = conn.getResponseCode();
        assertEquals("Unexpected response code,", expectedRC, rc);

        if (rc >= 200 && rc < 500) {
            try (InputStream is = conn.getInputStream()) {
                assertEquals(expected, slurpAsString(is));
            }
            finally {
                conn.disconnect();
            }
        }
        else {
            try (InputStream is = conn.getErrorStream()) {
                assertEquals(expected, slurpAsString(is));
            }
            finally {
                conn.disconnect();
            }
        }

    }

    protected static void assertContent(String expected, URL url) throws IOException {
        assertContent(200, expected, url);
    }

    protected static void assertResponseCode(int expected, URL url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            assertEquals(expected, conn.getResponseCode());
        }
        finally {
            conn.disconnect();
        }
    }

    protected static String slurpAsString(InputStream is) throws IOException {
        // See <weblogs.java.net/blog/pat/archive/2004/10/stupid_scanner_1.html>
        try (Scanner scanner = new Scanner(is, "UTF-8")) {
            scanner.useDelimiter("\\A");

            return scanner.hasNext() ? scanner.next() : null;
        }
    }

    /**
     * @return a "unique" context name, never <code>null</code>.
     */
    protected final String createUniqueContextName() {
        return "testContext" + System.currentTimeMillis();
    }

    /**
     * Called before each test, and can safely assume the HTTP service is available.
     * 
     * @see TestCase#setUp()
     */
    protected void doSetUp() throws Exception {
        // Nop
    }

    /**
     * Called after each test.
     * 
     * @see TestCase#tearDown()
     */
    protected void doTearDown() throws Exception {
        // Nop
    }

    protected Bundle getHttpBundle() {
        for (Bundle b : m_context.getBundles()) {
            if ("org.amdatu.http.jetty".equals(b.getSymbolicName())) {
                return b;
            }
        }
        fail("HTTP bundle not found?!");
        return null; // never reached...
    }

    protected Properties getHttpConfiguration() {
        Properties props = new Properties();
        props.put("host", "localhost");
        props.put("port", "8080");
        return props;
    }

    protected <T extends EventListener> ServiceRegistration registerEventListener(Class<T> type, T listener,
        Dictionary<String, ?> props) {
        ServiceRegistration registration = m_context.registerService(type.getName(), listener, props);
        m_managedServices.add(registration);
        return registration;
    }

    protected ServiceRegistration registerFilter(Filter filter, Dictionary<String, ?> props) {
        ServiceRegistration registration = m_context.registerService(Filter.class.getName(), filter, props);
        m_managedServices.add(registration);
        return registration;
    }

    protected ServiceRegistration registerServlet(Servlet servlet, Dictionary<String, ?> props) {
        ServiceRegistration registration = m_context.registerService(Servlet.class.getName(), servlet, props);
        m_managedServices.add(registration);
        return registration;
    }

    protected ServiceRegistration registerServletContextHelper(ServletContextHelper contextHelper,
        Dictionary<String, ?> props) {
        ServiceRegistration registration =
            m_context.registerService(ServletContextHelper.class.getName(), contextHelper, props);
        m_managedServices.add(registration);
        return registration;
    }

    @Override
    protected final void setUp() throws Exception {
        m_context = FrameworkUtil.getBundle(getClass()).getBundleContext();
        assertNotNull("Not running as OSGi unit test?!", m_context);

        if (!m_httpAvailable) {
            final CountDownLatch latch = new CountDownLatch(1);

            ServiceRegistration reg;
            Properties props = getHttpConfiguration();

            ServiceReference serviceRef = m_context.getServiceReference(ConfigurationAdmin.class.getName());
            assertNotNull("No ConfigAdmin found?!", serviceRef);

            final CountDownLatch configLatch = new CountDownLatch(1);

            reg = m_context.registerService(ConfigurationListener.class.getName(), new ConfigurationListener() {
                @Override
                public void configurationEvent(ConfigurationEvent event) {
                    if (FACTORY_PID.equals(event.getFactoryPid()) && (ConfigurationEvent.CM_UPDATED == event.getType())) {
                        configLatch.countDown();
                    }
                }
            }, null);

            ConfigurationAdmin configAdmin = (ConfigurationAdmin) m_context.getService(serviceRef);
            m_config = configAdmin.createFactoryConfiguration(FACTORY_PID, null);
            m_config.update(props);

            assertTrue(configLatch.await(5, TimeUnit.SECONDS));

            reg = m_context.registerService(ServletContextListener.class.getName(), new ServletContextListener() {
                @Override
                public void contextDestroyed(ServletContextEvent event) {
                    // Nop
                }

                @Override
                public void contextInitialized(ServletContextEvent event) {
                    latch.countDown();
                }
            }, null);

            m_httpAvailable = latch.await(5, TimeUnit.SECONDS);

            reg.unregister();

            assertTrue("HTTP service not available?!", m_httpAvailable);
        }

        Thread.sleep(50); // XXX needed to avoid a race upon starting Jetty...

        doSetUp();
    }

    @Override
    protected final void tearDown() throws Exception {
        if (m_config != null) {
            m_config.delete();
            m_config = null;
        }

        for (ServiceRegistration reg : m_managedServices) {
            try {
                reg.unregister();
            }
            catch (IllegalStateException e) {
                // Ignore; probably already unregistered...
            }
        }

        m_httpAvailable = false;

        doTearDown();
    }
}
