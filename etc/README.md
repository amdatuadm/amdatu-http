# Miscellaneous files

Used in integration tests, for project setup and so on:

- `cert/` contains a keystore with a self-signed certificate for testing 
  SSL-related stuff (HTTPS, SPDY);
- `npn-boot/` contains the npn-boot JARs needed for running SPDY.
