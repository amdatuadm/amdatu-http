# Amdatu-HTTP

This project is work in progress to deliver a more state-of-the-art HTTP
service implementation.

## Features

- Based on latest Jetty 9.1;
- support for whiteboard registration of:
  - filters;
  - servlets;
  - event listeners.
- preliminary implementation of RFC-189 (HTTP service update);
- add on for SPDY.

## Requirements

- Java 7;
- a correct npn-boot version for SPDY support.


