/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.demo;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Sample servlet for demonstration purposes.
 */
public class DemoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String encoding = "utf-8";
        resp.setCharacterEncoding(encoding);
        resp.setContentType("text/html");

        Locale locale = req.getLocale();

        String langParam = req.getParameter("lang");
        if (langParam != null) {
            locale = new Locale(langParam);
        }

        if (locale == null) {
            locale = Locale.ENGLISH;
        }

        Map<String, Locale> otherLocales = new LinkedHashMap<>();
        Enumeration e = req.getLocales();
        while (e.hasMoreElements()) {
            Locale l = (Locale) e.nextElement();
            otherLocales.put(l.getDisplayLanguage(locale), l);
        }
        otherLocales.remove(locale.getDisplayLanguage(locale));

        try (PrintWriter pw = new PrintWriter(resp.getOutputStream())) {
            pw.format(locale, "<html><head><meta charset=\"%1$s\"><title>%2$s</title></head>", encoding, getClass().getSimpleName());
            pw.format(locale, "<body><h1>Demo servlet</h1><p>Client language: %1$s.<br/>", locale.getDisplayLanguage(locale));
            if (!otherLocales.isEmpty()) {
                pw.format(locale, "Other languages are:");
                for (Entry<String, Locale> entry : otherLocales.entrySet()) {
                    pw.format(locale, "&#160;<a href=\"?lang=%2$s\">%1$s</a>", entry.getKey(), entry.getValue());
                }
                pw.format(locale, ".<br/>");
            }
            pw.format(locale, "At the server it is: <em>%1$tc</em></p></body></html>", new Date());
        }
    }
}
