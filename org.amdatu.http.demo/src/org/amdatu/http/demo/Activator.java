/*
 * Copyright (c) 2010-2014 - The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.http.demo;

import static org.amdatu.http.AmdatuHttpConstants.FACTORY_PID;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.Servlet;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.http.HttpConstants;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Bundle activator for OSGi.
 */
public class Activator implements BundleActivator {
    private static final String DEMO_TYPE = "org.amdatu.http.demo.type";

    private ServiceTracker m_tracker;

    @Override
    public void start(BundleContext context) throws Exception {
        Properties props = new Properties();
        props.put(HttpConstants.HTTP_WHITEBOARD_SERVLET_NAME, "DemoServlet");
        props.put(HttpConstants.HTTP_WHITEBOARD_SERVLET_PATTERN, "/");

        context.registerService(Servlet.class.getName(), new DemoServlet(), props);

        m_tracker = new ServiceTracker(context, ConfigurationAdmin.class.getName(), null) {
            @Override
            public Object addingService(ServiceReference reference) {
                ConfigurationAdmin configAdmin = (ConfigurationAdmin) super.addingService(reference);
                try {
                    String demoType = context.getProperty(DEMO_TYPE);
                    if (demoType == null || "".equals(demoType.trim())) {
                        demoType = "http";
                    }

                    Configuration config = configAdmin.createFactoryConfiguration(FACTORY_PID, null);
                    createDemoConfig(demoType, config);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                return configAdmin;
            }
        };
        m_tracker.open();
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        if (m_tracker != null) {
            m_tracker.close();
            m_tracker = null;
        }
    }

    private void createDemoConfig(String type, Configuration config) throws IOException {
        Properties props = new Properties();
        switch (type) {
            case "spdy":
                props.put("port", 8443);
                props.put("types", new String[] { "spdy" });
                props.put("keystore.path", "../etc/cert/server.jks");
                props.put("keystore.key.password", "secret");
                props.put("keystore.store.password", "secret");
                break;
            case "https":
                props.put("port", 8443);
                props.put("types", new String[] { "https" });
                props.put("keystore.path", "../etc/cert/server.jks");
                props.put("keystore.key.password", "secret");
                props.put("keystore.store.password", "secret");
                break;
            case "http":
            default:
                props.put("port", 8080);
                props.put("types", new String[] { "http" });
                break;
        }
        config.update(props);
    }
}
